var menuPrincipal;
var estructuraMenu;
var estructura;

function menu() {

    estructura = [];
    estructura[0] = ["2021_ciclo_II_ambisome_riesgo_ifi_00", [
            "2021_ciclo_II_ambisome_riesgo_ifi_00_00_portada",
            "2021_ciclo_II_ambisome_riesgo_ifi_00_01_aspergilosis_invasiva_aspergilosis",
            "2021_ciclo_II_ambisome_riesgo_ifi_00_02_candidiasis_invasiva_candidiasis",
        ]];

    if (location.host === "localhost") {
        edetailing.enableTestMode();
    }
    edetailing.debugBox = document.getElementById("returned-results");

    //edetailing.changeOverlayDefault("opacity", 0.3);
    //edetailing.changePopupDefault("temps", 300);

    edetailing.initCodi("", "np4");

    inicializar_navigate();

}
