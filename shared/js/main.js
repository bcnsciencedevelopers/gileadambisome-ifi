$(document).ready(function () {

    menu();
    main();
    espec();
});

function main() {

    /* * * * * * * * * * *
     *      Botones
     * * * * * * * * * * */

    // btn menu home
    $("#global").append("<div class='logo_home js-goto-touch' data-presentation='0' data-slide='0'></div>");
    // btn menu referencias
    // $("#global").append("<div class='logo_ref' onclick='openPop(\"popReferencias\")'></div>");
    
    if (contador_km == 1) {
        // btn popup 
        $("#global").append("<div id='btn_fugnica' class='btn_popup-1' onclick='openPop(\"popFugnica\")'></div>");
        $("#global").append("<div id='btn_ai' class='btn_popup-2' onclick='openPop(\"popAi\")'></div>");
    }

    if (contador_km == 2) {
        $("#global").append("<div id='btn_ci' class='btn_popup' onclick='openPop(\"popCi\")'></div>");
    }
    

    /* * * * * * * * * * *
     *      Popup
     * * * * * * * * * * */

    // popups referencias
    // $("#global").append("<div class='popup popup-fade' data-animacio='fade' id='popReferencias'>");
    // $("#popReferencias").append("<div class='btn-tancar' onclick='closePop(\"popReferencias\")'>");
    
    // Solo aparece en el slide 3: 'Evaluación general del paciente'
    if (contador_km == 1 ) {
        // popups infeccion fugnica
        $("#global").append("<div class='popup popup-fade' data-animacio='fade' id='popFugnica'>");
        $("#popFugnica").append("<div class='btn-tancar' onclick='closePop(\"popFugnica\")'>");
        
        // popups AI
        $("#global").append("<div class='popup popup-fade' data-animacio='fade' id='popAi'>");
        $("#popAi").append("<div class='btn-tancar' onclick='closePop(\"popAi\")'>");
    }

    if (contador_km == 2 ) {
        // popups Ci
        $("#global").append("<div class='popup popup-fade' data-animacio='fade' id='popCi'>");
        $("#popCi").append("<div class='btn-tancar' onclick='closePop(\"popCi\")'>");
    }
}

/****************
 FUNCIONES
 ****************/

function openPop(id) {
    $("#" + id).css('display', "block");
    $("#" + id).css('z-index', 999);
    $("#lightbox").css('z-index', 1);
    $("#lightbox").animate({
        opacity: 0.9
    }, edetailing.popupActual.dataTemps, function () {});
    $("#" + id).animate({
        opacity: 1
    }, edetailing.popupActual.dataTemps, function () {});
}

function closePop(id) {
    $("#lightbox").animate({
        opacity: 0
    }, edetailing.popupActual.dataTemps, function () {
        $("#lightbox").css('z-index', -50);
    });
    $("#" + id).animate({
        opacity: 0
    }, edetailing.popupActual.dataTemps, function () {
        $("#" + id).css('display', "none");
        $("#" + id).css('z-index', -50);
    });
}

/****************
 AE FUNCIONES
 ****************/

$("#sobre").on("click", function () {
    // alert('Enviamos template: ' + templateIdProd);
    // alert('Enviamos template: ' + templateIdTest);
    initFragments();
});

//test - entorno servidor test
//prod - entorno servidor producción
var entorno = 'prod';
var templateId = "";
var templateIdTest = 0;
var templateIdProd = 187381;
var vaultId = "";
var vaultIdTest = "...";    
var vaultIdProd = "https://gilead-eame.veevavault.com";
var emailFragmentsEnviar = [];
var approvedDocumentFragmentsId = [];
var templateApprovedId;
var contador = 0;
if (entorno === 'test') {
    vaultId = vaultIdTest;
    templateId = templateIdTest;
} else {
    vaultId = vaultIdProd;
    templateId = templateIdProd;
}
function initFragments() {
    console.log(templateId);
    console.log("init fragments");
    getTemplateDocument();
}

function getTemplateDocument() {
    com.veeva.clm.getApprovedDocument(vaultId, templateId, storeTemplateId);
}

//id de veeva de template
function storeTemplateId(result) {
    //    alert("template: " + JSON.stringify(result));
    templateApprovedId = result.Approved_Document_vod__c.ID;
    getApprovedDocuments({
        success: true
    });
}

//funcion recursiva que coje los ids de veeva de fragments
function getApprovedDocuments(result) {
    launchApprovedEmailFinal();
}

function launchApprovedEmailFinal() {
    //    alert("template Veeva ID: " + templateApprovedId);
    //    alert("fragments Veeva ID: " + approvedDocumentFragmentsId);
    com.veeva.clm.launchApprovedEmail(templateApprovedId, approvedDocumentFragmentsId, resultLaunch);
}

function resultLaunch(result) {
    if (!result.success) {
        // Informar a l'usuari que no ha seleccionat account i per tant no pot llanÃ§ar l'approved email
        //alert(result.success);
    }
}
/********************************
 Fin ae functions
 ********************************/